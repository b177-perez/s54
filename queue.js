let collection = [];
let head = 0;

// Write the queue functions below.

function print() {
	return collection;
}

function enqueue(element) {
	if(collection.length == 0){
		collection[0] = element;
	} else {
		collection[collection.length] = element;
	}
	return collection;
}

function dequeue() {
	let dequeued = [];

	for(let i = 0; i < collection.length-1; i++) {
		dequeued[i] = collection[i+1];
	}

	collection = dequeued;

	return collection;
}

function front() {
	return collection[head];
}

function size() {
	return collection.length;
}

function isEmpty () {
	return size() === 0;
}

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};
